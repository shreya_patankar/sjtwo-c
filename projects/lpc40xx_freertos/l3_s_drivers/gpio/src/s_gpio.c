#include "s_gpio.h"
#include "lpc40xx.h"

static LPC_GPIO_TypeDef *const LPC_GPIO[5] = {LPC_GPIO0, LPC_GPIO1, LPC_GPIO2, LPC_GPIO3, LPC_GPIO4};

void gpio_init(uint8_t gpio_port_number, uint8_t gpio_pin_number, bool is_output) {

  if (is_output) {
    LPC_GPIO[gpio_port_number]->DIR |= (1 << gpio_pin_number);
  } else {
    LPC_GPIO[gpio_port_number]->DIR &= ~(1 << gpio_pin_number);
  }
}

void gpio_set(uint8_t gpio_port_number, uint8_t gpio_pin_number) {
  LPC_GPIO[gpio_port_number]->PIN |= (1 << gpio_pin_number);
}

void gpio_reset(uint8_t gpio_port_number, uint8_t gpio_pin_number) {
  LPC_GPIO[gpio_port_number]->PIN &= ~(1 << gpio_pin_number);
}

void gpio_toggle(uint8_t gpio_port_number, uint8_t gpio_pin_number) {
  LPC_GPIO[gpio_port_number]->PIN ^= (1 << gpio_pin_number);
}

bool gpio_check(uint8_t gpio_port_number, uint8_t gpio_pin_number) {
  if (LPC_GPIO[gpio_port_number]->PIN & (1 << gpio_pin_number))
    return true;
  else
    return false;
}