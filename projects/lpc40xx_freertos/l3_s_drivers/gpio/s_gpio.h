#include <stdbool.h>
#include <stdint.h>

typedef enum {
  GPIO_PORT_0 = 0,
  GPIO_PORT_1,
  GPIO_PORT_2,
  GPIO_PORT_3,
  GPIO_PORT_4,
  GPIO_PORT_5,
} s_gpio__port_e;

typedef struct {
  s_gpio__port_e port_number;
  uint8_t pin_number;
} s_gpio_s;

void gpio_init(uint8_t gpio_port_number, uint8_t gpio_pin_number, bool is_output);

void gpio_set(uint8_t gpio_port_number, uint8_t gpio_pin_number);

void gpio_reset(uint8_t gpio_port_number, uint8_t gpio_pin_number);

void gpio_toggle(uint8_t gpio_port_number, uint8_t gpio_pin_number);

bool gpio_check(uint8_t gpio_port_number, uint8_t gpio_pin_number);