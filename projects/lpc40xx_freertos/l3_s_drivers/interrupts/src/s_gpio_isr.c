#include "s_gpio_isr.h"
#include "FreeRTOS.h"
#include "lpc40xx.h"
#include "s_gpio.h"
#include <stdio.h>
static function_pointer_t gpio0_rise_callbacks[32];
static function_pointer_t gpio2_rise_callbacks[32];

void gpio__attach_interrupt(uint32_t port_number, uint32_t pin, function_pointer_t callback) {

  if (port_number == 0) {
    gpio0_rise_callbacks[pin] = callback;
  } else if (port_number == 2) {
    gpio2_rise_callbacks[pin] = callback;
  }
  // 1) Store the callback based on the pin at gpio0_callbacks
  // 2) Configure GPIO 0 pin for rising or falling edge
}

s_gpio_s interrupt_pin() {

  s_gpio_s iterrupted_pin_port;

  if ((LPC_GPIOINT->IntStatus & (1 << 0))) {
    iterrupted_pin_port.port_number = 0;
  } else if ((LPC_GPIOINT->IntStatus & (1 << 2))) {
    iterrupted_pin_port.port_number = 2;
  }

  if (LPC_GPIOINT->IO0IntStatF) {
    for (int i = 0; i < 32; i++) {
      if (LPC_GPIOINT->IO0IntStatF & (1 << i)) {
        iterrupted_pin_port.pin_number = i;
        break;
      }
    }
  } else if (LPC_GPIOINT->IO0IntStatR) {
    for (int i = 0; i < 32; i++) {
      if (LPC_GPIOINT->IO0IntStatR & (1 << i)) {
        iterrupted_pin_port.pin_number = i;
        break;
      }
    }
  } else if (LPC_GPIOINT->IO2IntStatR) {
    for (int i = 0; i < 32; i++) {
      if (LPC_GPIOINT->IO2IntStatR & (1 << i)) {
        iterrupted_pin_port.pin_number = i;
        break;
      }
    }
  } else if (LPC_GPIOINT->IO2IntStatF) {
    for (int i = 0; i < 32; i++) {
      if (LPC_GPIOINT->IO2IntStatF & (1 << i)) {
        iterrupted_pin_port.pin_number = i;
        break;
      }
    }
  }
  return iterrupted_pin_port;
}

void clear_pin_interrupt(s_gpio_s port_pin_to_clear) {
  if (port_pin_to_clear.port_number == 2) {
    LPC_GPIOINT->IO2IntClr |= (1 << port_pin_to_clear.pin_number);
    fprintf(stderr, "clear interrupt for 0\n");
  } else if (port_pin_to_clear.port_number == 0) {
    LPC_GPIOINT->IO0IntClr |= (1 << port_pin_to_clear.pin_number);

    fprintf(stderr, "clear interrupt for 2 = %d\n", port_pin_to_clear.pin_number);
  }
}

// We wrote some of the implementation for you
void gpio0__interrupt_dispatcher(void) {
  // Check which pin generated the interrupt
  const s_gpio_s pin_that_generated_interrupt = interrupt_pin();
  // fprintf(stderr, "interrupted pin%d\n", pin_that_generated_interrupt.pin_number);
  // fprintf(stderr, "interrupted port%d\n", pin_that_generated_interrupt.port_number);
  function_pointer_t attached_user_handler;
  if (pin_that_generated_interrupt.port_number == 0) {
    attached_user_handler = gpio0_rise_callbacks[pin_that_generated_interrupt.pin_number];
  } else if (pin_that_generated_interrupt.port_number == 2) {
    attached_user_handler = gpio2_rise_callbacks[pin_that_generated_interrupt.pin_number];
  }

  // Invoke the user registered callback, and then clear the interrupt
  attached_user_handler();
  clear_pin_interrupt(pin_that_generated_interrupt);
}