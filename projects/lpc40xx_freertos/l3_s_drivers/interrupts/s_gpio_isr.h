#include <stdint.h>

typedef void (*function_pointer_t)(void);

void gpio__attach_interrupt(uint32_t port_number, uint32_t pin_number, function_pointer_t callback);
