#include "s_spi.h"
#include "lpc40xx.h"
#include "s_gpio.h"

void sspi2__init(uint32_t max_clock_mhz) {
  // Refer to LPC User manual and setup the register bits correctly
  // a) Power on Peripheral
  LPC_SC->PCONP |= (1 << 20);
  // b) Setup control registers CR0 and CR1
  LPC_IOCON->P1_0 = (0b100 << 0);
  LPC_IOCON->P1_1 = (0b100 << 0);
  LPC_IOCON->P1_4 = (0b100 << 0);
  LPC_SSP2->CR0 |= (7 << 0);
  // c) Setup prescalar register to be <= max_clock_mhz
  gpio_init(1, 10, 1);
  LPC_SSP2->CPSR = (A_CPU_CLOCK_FREQUENCY / (1000 * 1000)) / max_clock_mhz;
}

uint8_t sspi2__exchange_byte(uint8_t data_out) {
  // Configure the Data register(DR) to send and receive data by checking the SPI peripheral status register
  // printf("above while 1\n");
  while ((LPC_SSP2->SR & (1 << 0)) == 0)
    ;
  // printf("below while 1\n");
  LPC_SSP2->DR = data_out;

  // printf("above while 2\n");
  while ((LPC_SSP2->SR & (1 << 2)) == 0)
    ;
  // printf("below while 2\n");
  data_out = (LPC_SSP2->DR & 0xFF);

  return data_out;
}

void adesto_cs(void) { gpio_reset(1, 10); }
void adesto_ds(void) { gpio_set(1, 10); }

adesto_flash_id_s adesto_read_signature(void) {
  adesto_flash_id_s data = {0};

  adesto_cs();
  {
    sspi2__exchange_byte(0x9F);
    data.manufacturer_id = sspi2__exchange_byte(0xFF);
    printf("data manufacturer id in func %x\n", data.manufacturer_id);

    data.device_id_1 = sspi2__exchange_byte(0xFF);

    printf("data device id 1 in func %x\n", data.device_id_1);

    data.device_id_2 = sspi2__exchange_byte(0xFF);

    printf("data device id 2 in func %x\n", data.device_id_2);

    data.extended_device_id = sspi2__exchange_byte(0xFF);
    printf("data extended id in func%x\n", data.extended_device_id);

    // Send opcode and read bytes
    // TODO: Populate members of the 'adesto_flash_id_s' struct
  }
  adesto_ds();

  return data;
}
