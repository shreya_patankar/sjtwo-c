#include <stdint.h>

#define A_CPU_CLOCK_FREQUENCY 96 * 1000 * 1000

typedef struct {
  uint8_t manufacturer_id;
  uint8_t device_id_1;
  uint8_t device_id_2;
  uint8_t extended_device_id;
} adesto_flash_id_s;

void sspi2__init(uint32_t max_clock_mhz);

uint8_t sspi2__exchange_byte(uint8_t data_out);

void adesto_cs(void);
void adesto_ds(void);

adesto_flash_id_s adesto_read_signature(void);