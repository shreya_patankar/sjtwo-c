#include <stdio.h>

#include "FreeRTOS.h"
#include "task.h"

#include "board_io.h"
#include "common_macros.h"
#include "periodic_scheduler.h"
#include "s_gpio.h"
#include "s_gpio_isr.h"
#include "s_spi.h"
#include "semphr.h"
#include "sj2_cli.h"

// 'static' to make these functions 'private' to this file
static void create_blinky_tasks(void);
static void create_uart_task(void);
static void blink_task(void *params);
static void uart_task(void *params);

static SemaphoreHandle_t sem_for_switch;
SemaphoreHandle_t spi_bus_mutex;
void led_1_blink_task(void *p) {
  gpio_s led1;
  led1.pin_number = 3;
  led1.port_number = 2;

  gpio_init(led1.port_number, led1.pin_number, 1);
  while (1) {
    gpio_set(led1.port_number, led1.pin_number);
    vTaskDelay(500);
    gpio_reset(led1.port_number, led1.pin_number);
    vTaskDelay(500);
  }
}

/*
void switch_using_interrupt() {
  gpio_s switch_1, led1;
  switch_1.pin_number = 30;
  switch_1.port_number = 0;

  led1.pin_number = 3;
  led1.port_number = 2;

  gpio_init(led1.port_number, led1.pin_number, 1);
  gpio_init(switch_1.port_number, switch_1.pin_number, 0);
  LPC_GPIOINT->IO0IntEnR |= (1 << switch_1.pin_number);
}

*/

void switch_isr(void) {
  fprintf(stderr, "interrupted pin\n");
  xSemaphoreGiveFromISR(sem_for_switch, NULL);
}

void switch_isr_using_semaphore(void *p) {

  gpio_s led1;
  led1.pin_number = 3;
  led1.port_number = 2;
  gpio_init(led1.port_number, led1.pin_number, 1);

  while (1) {
    if (xSemaphoreTake(sem_for_switch, portMAX_DELAY)) {
      gpio_set(led1.port_number, led1.pin_number);
      vTaskDelay(500);
      gpio_reset(led1.port_number, led1.pin_number);
      vTaskDelay(500);
    }
  }
}

void led_switch(void *p) {
  gpio_s led1;
  gpio_s switch1;

  led1.pin_number = 24;
  led1.port_number = 1;
  switch1.pin_number = 30;
  switch1.port_number = 0;
  gpio_init(led1.port_number, led1.pin_number, 1);
  gpio_init(switch1.port_number, switch1.pin_number, 0);

  while (1) {
    while (gpio_check(switch1.port_number, switch1.pin_number)) {
      gpio_toggle(led1.port_number, led1.pin_number);
      vTaskDelay(500);
    }

    gpio_set(led1.port_number, led1.pin_number);
  }
}

void spi_task(void *p) {

  const uint32_t spi_clock = 24;
  sspi2__init(spi_clock);

  while (1) {
    if (xSemaphoreTake(spi_bus_mutex, 1000)) {
      adesto_flash_id_s data_members = adesto_read_signature();

      fprintf(stderr, "data manufacturer id %x\n", data_members.manufacturer_id);
      fprintf(stderr, "data device id 1 %x\n", data_members.device_id_1);
      fprintf(stderr, "data device id 2 %x\n", data_members.device_id_2);
      fprintf(stderr, "data extended id %x\n", data_members.extended_device_id);

      vTaskDelay(1000);
      xSemaphoreGive(spi_bus_mutex);
    }
  }
}

int main(void) {
  // create_blinky_tasks();
  spi_bus_mutex = xSemaphoreCreateMutex();
  create_uart_task();
  // gpio_init(0, 30, 0);
  // LPC_GPIOINT->IO0IntEnR |= (1 << 30);
  // sem_for_switch = xSemaphoreCreateBinary();
  // switch_using_interrupt();
  // gpio_set(2, 3);
  // gpio__attach_interrupt(0, 30, switch_isr);
  // NVIC_EnableIRQ(GPIO_IRQn);
  // xTaskCreate(switch_isr_using_semaphore, "led_1", 400 / sizeof(void *), NULL, PRIORITY_LOW, NULL);
  xTaskCreate(spi_task, "spi_2", 4000 / sizeof(void *), NULL, PRIORITY_LOW, NULL);
  xTaskCreate(spi_task, "spi_2", 4000 / sizeof(void *), NULL, PRIORITY_LOW, NULL);
  // If you have the ESP32 wifi module soldered on the board, you can try uncommenting this code
  // See esp32/README.md for more details
  // uart3_init();                                                                     // Also include:  uart3_init.h
  // xTaskCreate(esp32_tcp_hello_world_task, "uart3", 1000, NULL, PRIORITY_LOW, NULL); // Include esp32_task.h

  // xTaskCreate(led_1_blink_task, "led_1", 400 / sizeof(void *), NULL, PRIORITY_LOW, NULL);
  // xTaskCreate(led_switch, "led__sitch", 400 / sizeof(void *), NULL, PRIORITY_LOW, NULL);
  // puts("Starting RTOS");
  vTaskStartScheduler(); // This function never returns unless RTOS scheduler runs out of memory and fails

  return 0;
}

static void create_blinky_tasks(void) {
  /**
   * Use '#if (1)' if you wish to observe how two tasks can blink LEDs
   * Use '#if (0)' if you wish to use the 'periodic_scheduler.h' that will spawn 4 periodic tasks, one for each LED
   */
#if (1)
  // These variables should not go out of scope because the 'blink_task' will reference this memory
  static gpio_s led0, led1;

  led0 = board_io__get_led0();
  led1 = board_io__get_led1();

  xTaskCreate(blink_task, "led0", configMINIMAL_STACK_SIZE, (void *)&led0, PRIORITY_LOW, NULL);
  xTaskCreate(blink_task, "led1", configMINIMAL_STACK_SIZE, (void *)&led1, PRIORITY_LOW, NULL);
#else
  const bool run_1000hz = true;
  const size_t stack_size_bytes = 2048 / sizeof(void *); // RTOS stack size is in terms of 32-bits for ARM M4 32-bit CPU
  periodic_scheduler__initialize(stack_size_bytes, !run_1000hz); // Assuming we do not need the high rate 1000Hz task
  UNUSED(blink_task);
#endif
}

static void create_uart_task(void) {
  // It is advised to either run the uart_task, or the SJ2 command-line (CLI), but not both
  // Change '#if (0)' to '#if (1)' and vice versa to try it out
#if (0)
  // printf() takes more stack space, size this tasks' stack higher
  xTaskCreate(uart_task, "uart", (512U * 8) / sizeof(void *), NULL, PRIORITY_LOW, NULL);
#else
  sj2_cli__init();
  UNUSED(uart_task); // uart_task is un-used in if we are doing cli init()
#endif
}

static void blink_task(void *params) {
  const gpio_s led = *((gpio_s *)params); // Parameter was input while calling xTaskCreate()

  // Warning: This task starts with very minimal stack, so do not use printf() API here to avoid stack overflow
  while (true) {
    gpio__toggle(led);
    vTaskDelay(500);
  }
}

// This sends periodic messages over printf() which uses system_calls.c to send them to UART0
static void uart_task(void *params) {
  TickType_t previous_tick = 0;
  TickType_t ticks = 0;

  while (true) {
    // This loop will repeat at precise task delay, even if the logic below takes variable amount of ticks
    vTaskDelayUntil(&previous_tick, 2000);

    /* Calls to fprintf(stderr, ...) uses polled UART driver, so this entire output will be fully
     * sent out before this function returns. See system_calls.c for actual implementation.
     *
     * Use this style print for:
     *  - Interrupts because you cannot use printf() inside an ISR
     *    This is because regular printf() leads down to xQueueSend() that might block
     *    but you cannot block inside an ISR hence the system might crash
     *  - During debugging in case system crashes before all output of printf() is sent
     */
    ticks = xTaskGetTickCount();
    fprintf(stderr, "%u: This is a polled version of printf used for debugging ... finished in", (unsigned)ticks);
    fprintf(stderr, " %lu ticks\n", (xTaskGetTickCount() - ticks));

    /* This deposits data to an outgoing queue and doesn't block the CPU
     * Data will be sent later, but this function would return earlier
     */
    ticks = xTaskGetTickCount();
    printf("This is a more efficient printf ... finished in");
    printf(" %lu ticks\n\n", (xTaskGetTickCount() - ticks));
  }
}
